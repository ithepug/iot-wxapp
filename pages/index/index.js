//index.js
//获取应用实例
const app = getApp()

var page

Page({
  data:{
    lightimagepath:"/images/light_off.png",
    temperature:"--",
    humidity:"--"
  },
  onLoad: function(options) {
    page=this
  },
  LightOn: function(event)
  {
    console.log(event)

    // 判断是否已成功连接
    if (!app.globalData.client.connected) {
      console.log('客户端未连接')
      return
    }

    // publich(topic, payload, options/callback)
    app.globalData.client.publish('device/0/light', '1', (error) => {
      console.log(error || '消息发布成功')
    })

  },
  LightOff: function(event)
  {
    console.log(event)

    if (!app.globalData.client.connected) {
      console.log('客户端未连接')
      return
    }

    // publich(topic, payload, options/callback)
    app.globalData.client.publish('device/0/light', '0', (error) => {
      console.log(error || '消息发布成功')
    })    
  }
})

app.globalData.client.on('message', (topic, message) => {
  console.log('收到来自', topic, '的消息:', message.toString())
  if(topic == 'device/0/light')
  {
    if(message==1)
    {
      page.setData({
        lightimagepath:"/images/light_on.png"
      })
      console.log('on')
    }
    else
    {
      page.setData({
        lightimagepath:"/images/light_off.png"
      })
      console.log('off')
    }
  }
  else if(topic == 'device/0/temperature')
  {
    page.setData({
      temperature:message.toString()
    })
  }
  else if(topic == 'device/0/humidity')
  {
    page.setData({
      humidity:message.toString()
    })
  }
})
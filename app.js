import mqtt from './utils/mqtt.js';

App({
  onLaunch: function () {

    const host = 'wxs://iot.code365.tech:8084/wxapp';
    var options = {
      connectTimeout: 4000,
      clientId: 'no_login',
      keepalive: 60,
      clean: true,
    };

    //连接MQTT broker
    const client = mqtt.connect(host, options);

    client.on('connect', () => {
      console.log('连接成功')

      client.subscribe(
        ['device/0/light', 'device/0/temperature', 'device/0/humidity'], 
        { qos: 1 },  
        (err) => {
          console.log(err || '订阅成功')
        },
      )
    })
  
    client.on('reconnect', (error) => {
        console.log('正在重连:', error)
    })
  
    client.on('error', (error) => {
        console.log('连接失败:', error)
    })
    this.globalData.client=client
  },
  globalData: {
    userInfo: null,
    client: null
  }
})